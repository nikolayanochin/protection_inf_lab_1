<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 11.10.2018
 * Time: 16:34
 */
ini_set('memory_limit', '1024M');
require_once('autoload.php');

use lib\Cryptographer;
use lib\Decryptor;

$cryptographer = new Cryptographer(__DIR__ . '/docs/1section.txt', __DIR__ . '/output/crypt.txt');
$cryptographer->crypt(1);

$decriptor = new Decryptor(__DIR__ . '/output/crypt.txt', __DIR__ . '/output/encrypt.txt', __DIR__ . '/docs/all.txt');
$decriptor->frequency_decrypt();

$decriptor->input_file = file_get_contents(__DIR__ . '/output/encrypt.txt');
$decriptor->input_file_path = __DIR__ . '/output/encrypt.txt';
//
$decriptor->frequencyAB_decrypt();

// TODO нужно дописать в это гавнище, чтобы при частотном анализе происходила замена только 100% увереных символов
// TODO Потом брать биграммы со 100% уверенными символами или самые частые вообще хз как это должно работать
