<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 11.10.2018
 * Time: 16:35
 */


namespace lib;

/**
 * Class Cryptographer
 * @package lib
 */
class Cryptographer extends CryptoHelper
{
    private $shifted_alphabet = [];

    /** @inheritdoc */
    public function __construct($input_file_path, $output_file_path)
    {
        parent::__construct($input_file_path, $output_file_path);
    }


    /**
     * Ширфуем наше сообщение и записываем в файл
     * @param int $shift
     */
    public function crypt($shift = 10)
    {
        $this->setShiftedAlphabet($shift);

        $chars = preg_split('//u', $this->input_file);

        $c = 0;
        foreach ($chars as $char) {
            $upper = false;
            if (preg_match("/[A-ZА-Я]/u", $char) != 0) {
                $char = mb_strtolower($char, 'UTF-8');
                $upper = true;
            }
            if (($i = array_search($char, $this->alphabet)) !== false) {
                $chars[$c] = $upper ? mb_strtoupper($this->shifted_alphabet[$i], 'UTF-8') : $this->shifted_alphabet[$i];
            }
            $c++;
        }

        $output = implode('', $chars);

        file_put_contents($this->output_file_path, $output);
    }

    /**
     * Устанавливаем сдвинутый алфавит
     * @param $shift
     */
    private function setShiftedAlphabet($shift)
    {
        $alphabet_count = count($this->alphabet);

        for ($c = 0; $c < $alphabet_count; $c++) {
            $last_num = $c - $shift;
            if ($last_num < 0) {
                $last_num = $alphabet_count - $shift + $c;
            }
            $last_value = $this->alphabet[$last_num];
            $this->shifted_alphabet[$c] = $last_value;
        }
    }
}