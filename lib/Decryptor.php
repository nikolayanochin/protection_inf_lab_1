<?php
/**
 * Created with love by АльянсЭкспресс.
 * Author: Anochin Nikolay
 * Email: titan12345@mail.ru
 * Phone: +7 925 174 1314
 * Date: 11.10.2018
 * Time: 17:52
 */


namespace lib;

/**
 * Class Decryptor
 * @package lib
 */
class Decryptor extends CryptoHelper
{
    protected $teaching_file;
    private $count_chars = [];
    private $count_charsAB = [];
    private $trained_count = [];
    private $trained_countAB = [];
    private $text_length = 0;
    private $input_chars = [];

    private $matching = [];
    private $matchingAB = [];

    private $multiplication_array = [];

    /** @inheritdoc */
    public function __construct($input_file_path, $output_file_path, $teaching_file)
    {
        parent::__construct($input_file_path, $output_file_path);
        $this->teaching_file = $teaching_file;
        $this->input_file_path = $input_file_path;
    }


    /**
     * Выполняем расшифровку частотным анализом
     */
    public function frequency_decrypt()
    {
        $this->getCountChars();

        foreach ($this->trained_frequency as $key => $frequency) {
            $this->trained_count[$key] = $frequency * $this->text_length;
        }

        arsort($this->trained_count);
        arsort($this->count_chars);

        $this->matching = [];

        $c = 0;
        $keys = array_keys($this->count_chars);
        foreach ($this->trained_count as $key => $letter_num) {
            $this->matching[$keys[$c]] = $key;
            $c++;
        }

        ksort($this->matching);

        $output_chars = $this->input_chars;

        foreach ($this->input_chars as $key => $char) {
            $upper = false;
            if (preg_match("/[A-ZА-Я]/u", $char) != 0) {
                $char = mb_strtolower($char, 'UTF-8');
                $upper = true;
            }
            if (($i = array_search($char, $this->alphabet)) !== false) {
                $new_letter = $this->alphabet[$this->matching[$i]];
                $output_chars[$key] = $upper ? mb_strtoupper($new_letter, 'UTF-8') : $new_letter;
            }
        }
        file_put_contents($this->output_file_path, implode('', $output_chars));
    }


    /**
     * Подсчет всех символов в исходном тексте
     */
    private function getCountChars()
    {
        $this->input_chars = preg_split('//u', $this->input_file);

        foreach ($this->input_chars as $char) {
            $char = mb_strtolower($char, 'UTF-8');
            if (($i = array_search($char, $this->alphabet)) !== false) {
                $this->count_chars[$i] = !empty($this->count_chars[$i]) ? $this->count_chars[$i] + 1 : 1;
                $this->text_length++;
            }
        }
    }

    public function frequencyAB_decrypt()
    {
        $this->setMultiplicationAlphabet();
        $this->getCountCharsAB();

        foreach ($this->trained_frequencyAB as $key => $frequency) {
            $this->trained_countAB[$key] = ($frequency * $this->text_length);
        }

        arsort($this->trained_countAB);
        arsort($this->count_charsAB);

        $matching = [];

        $c = 0;

        foreach ($this->trained_countAB as $key => $value) {
            if($key > 10) {
                unset($this->count_charsAB[$key]);
            }
        }

        $keys = array_keys($this->count_charsAB);
        foreach ($this->trained_countAB as $key => $letter_num) {
            if ($c > 9) {
                break;
            }
            $matching[$keys[$c]] = $key;
            $c++;
        }

        ksort($matching);

        $output_chars = $this->input_chars = preg_split('//u', file_get_contents($this->input_file_path));

        foreach ($this->input_chars as $key => $char) {
            $upper = false;
            if (preg_match("/[A-ZА-Я]/u", $char) != 0) {
                $char = mb_strtolower($char, 'UTF-8');
                $upper = true;
            }
            if (
                ($i = array_search($char, $this->alphabet)) !== false
                && ($b = array_search($this->input_chars[$key + 1], $this->alphabet)) !== false
            ) {
                $group = $char . $this->input_chars[$key + 1];
                if (!key_exists($group, $matching)) {
                    continue;
                }
                $new_group = $matching[$group];
                $new_char1 = mb_substr($new_group, 0, 1);
                $new_char2 = mb_substr($new_group, 1, 1);
                $output_chars[$key] = $upper ? mb_strtoupper($new_char1, 'UTF-8') : $new_char1;
                $output_chars[$key + 1] = $new_char2;
            }
        }
        file_put_contents($this->output_file_path, implode('', $output_chars));
    }

    /**
     * Декартово произведение алфавита
     */
    private function setMultiplicationAlphabet()
    {
        foreach ($this->alphabet as $letter) {
            foreach ($this->alphabet as $letter2) {
                $this->multiplication_array[] = $letter . $letter2;
            }
        }
    }

    /**
     * Подсчет всех символов в исходном тексте
     */
    private function getCountCharsAB()
    {
        $c = 0;

        $handle = @fopen($this->input_file_path, "r");
        if ($handle) {
            while (($buffer = fgets($handle, 4096)) !== false) {
                $buffer = mb_strtolower($buffer, 'UTF-8');
                $strlen = iconv_strlen($buffer);
                for ($e = 0; $e < $strlen; $e += 2) {
                    $char1 = mb_substr($buffer, $e, 1);
                    $char2 = mb_substr($buffer, $e + 1, 1);
                    if (
                        ($i = array_search($char1, $this->alphabet)) !== false
                        && ($b = array_search($char2, $this->alphabet)) !== false
                    ) {
                        $this->count_charsAB[$char1 . $char2] = !empty($this->count_charsAB[$char1 . $char2]) ? $this->count_charsAB[$char1 . $char2] + 1 : 1;
                        $this->text_length++;
                    }
                }
            }
            if (!feof($handle)) {
                echo "Ошибка: fgets() неожиданно потерпел неудачу\n";
            }
            fclose($handle);
        }
    }
}